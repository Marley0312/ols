#!/bin/bash

for PLATFORM in "linux/386" "linux/amd64" "darwin/arm64" "darwin/amd64" "windows/amd64" "windows/386"; do
    GOOS=${PLATFORM%/*}
    GOARCH=${PLATFORM#*/}

    BIN_FILE_NAME="binaries/ols-${GOOS}-${GOARCH}"

    if [[ "${GOOS}" == "windows" ]]; then
        BIN_FILE_NAME="${BIN_FILE_NAME}.exe"
    fi

    echo Building ${GOOS}-${GOARCH}...

    GOOS="${GOOS}" GOARCH="${GOARCH}" go build -o "${BIN_FILE_NAME}" || FAILURES="${FAILURES} ${GOOS}-${GOARCH}"
done

echo Build Failures:

for FAILURE in $FAILURES; do
    echo $FAILURE
done
