package convert

import (
	"log"
	"strconv"
	"strings"
)

var abilityMap = map[string]string{
	"right_foot":          "Rechter Fuß",
	"left_foot":           "Linker Fuß",
	"shooting":            "Schusstechnik",
	"power":               "Schusskraft",
	"technique":           "Technik",
	"pace":                "Schnelligkeit",
	"heading":             "Kopfball",
	"tackling":            "Zweikampf",
	"constitution":        "Athletik",
	"condition":           "Kondition",
	"gameInsight":         "Taktikverst.",
	"keeper_on_the_line":  "Linie",
	"keeper_off_the_line": "Rauslaufen",
	"keeper_box":          "Strafraum",
	"keeper_foot":         "Fuss",
	"keeper_game_opening": "Spieleröffnung",
	"keeper_sweeper":      "Libero",
}

func AbilityToFloat64(value string) float64 {
	value = strings.Replace(value, "%", "", -1)
	valueAsFloat64, err := strconv.ParseFloat(value, 64)
	if err != nil {
		//debug log
		// log.Println(err, value)
		return 0
	}
	return valueAsFloat64
}

func TalentToFloat64(value string) float64 {
	if strings.Contains(value, "nicht") {
		return 0
	}

	splits := strings.Split(value, "%")
	value = splits[0]

	valueAsFloat64, err := strconv.ParseFloat(value, 64)
	if err != nil {
		log.Println(err)
		return 0
	}
	return valueAsFloat64
}

func AbilityMapToAbilities(input map[string]string) map[string]float64 {
	formattedMap := make(map[string]float64)
	for key, value := range abilityMap {
		formattedMap[key] = AbilityToFloat64(input[value])
	}

	//debug log
	// log.Println(utils.JSONPrint(formattedMap))

	return formattedMap
}
