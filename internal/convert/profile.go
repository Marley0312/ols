package convert

import (
	"log"
	"strconv"
	"strings"
)

func ProfileValueToFloat64(value string, delimiter string) float64 {
	value = strings.Replace(value, ",", ".", -1)
	value = strings.Replace(value, delimiter, "", -1)
	valueAsFloat64, err := strconv.ParseFloat(value, 64)
	if err != nil {
		log.Println(err)
		return 0
	}
	return valueAsFloat64
}

func ProfileValueToInt32(value string, delimiter string) int {
	value = strings.Replace(value, ",", ".", -1)
	value = strings.Replace(value, delimiter, "", -1)
	valueAsInt64, err := strconv.ParseInt(value, 0, 32)
	if err != nil {
		log.Println(err)
		return 0
	}
	return int(valueAsInt64)
}

func CurrencyToInt32(value string) int {
	value = strings.Replace(value, " €", "", -1)
	value = strings.Replace(value, ".", "", -1)

	valueAsInt64, err := strconv.ParseInt(value, 0, 32)
	if err != nil {
		log.Println(err)
		return 0
	}
	return int(valueAsInt64)
}

func CurrencyToFloat64(value string) float64 {
	value = strings.Replace(value, " €", "", -1)
	value = strings.Replace(value, ".", "", -1)

	valueAsFloat64, err := strconv.ParseFloat(value, 64)
	if err != nil {
		log.Println(err)
		return 0
	}
	return valueAsFloat64
}
