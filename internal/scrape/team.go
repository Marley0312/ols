package scrape

import (
	"fmt"
	"net/http"

	"github.com/PuerkitoBio/goquery"
	"github.com/tripleawwy/ols/internal/onlineliga"
	"github.com/tripleawwy/ols/internal/request"
	"github.com/tripleawwy/ols/internal/response"
)

func FetchTeamDocument(team *onlineliga.Team, navigation string) *goquery.Document {
	teamSquadOptions := &request.RequestOption{
		Path: fmt.Sprintf("/team/overview/%s", navigation),
		QueryParameters: map[string]string{
			"userId": fmt.Sprintf("%v", team.ID),
		},
	}
	res := request.DoRequest(http.DefaultClient, request.GET(teamSquadOptions))
	doc := response.ResponseToDocument(res)
	return doc
}
