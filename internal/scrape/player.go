package scrape

import (
	"fmt"
	"net/http"

	"github.com/PuerkitoBio/goquery"
	"github.com/tripleawwy/ols/internal/onlineliga"
	"github.com/tripleawwy/ols/internal/request"
	"github.com/tripleawwy/ols/internal/response"
)

func FetchPlayerDocument(player *onlineliga.Player, navigation string) *goquery.Document {
	playerOptions := &request.RequestOption{
		Path: fmt.Sprintf("/player/%s", navigation),
		QueryParameters: map[string]string{
			"playerId": fmt.Sprintf("%v", player.ID),
		},
	}
	res := request.DoRequest(http.DefaultClient, request.GET(playerOptions))
	doc := response.ResponseToDocument(res)
	return doc
}
