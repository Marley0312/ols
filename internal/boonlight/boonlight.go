package boonlight

import (
	"encoding/csv"
	"log"
	"os"
	"strconv"

	"github.com/tripleawwy/ols/internal/convert"
	"github.com/tripleawwy/ols/internal/onlineliga"
	"github.com/tripleawwy/ols/internal/utils"
)

func ToCSV(leagueName string, team *onlineliga.Team) {
	csvFile, err := os.OpenFile("./data.csv", os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0644)
	if err != nil {
		csvFile, err = os.Create("./data.csv")
	}

	if err != nil {
		log.Println(err)
	}
	defer csvFile.Close()

	writer := csv.NewWriter(csvFile)
	writer.Comma = '\t'
	writer.Write(SPrint(leagueName, team))
	writer.Flush()
}

func SPrint(leagueName string, team *onlineliga.Team) []string {

	mvSum, mvAvg := team.MarketValue()
	salariesSum, salariesAvg := team.Salary()
	_, ageAvg := team.Age()
	expiringContracts := team.ExpiringContracts()

	top15 := team.Top15Players()
	_, t15TalentAvg := talent(top15)
	_, t15AgeAvg := age(top15)
	_, t15OverallAvg := overall(top15)

	tw := team.PlayersByPosition("Torwart")
	av := team.PlayersByPosition("Außenverteidiger")
	iv := team.PlayersByPosition("Innenverteidiger")
	dm := team.PlayersByPosition("Defensives Mittelfeld")
	om := team.PlayersByPosition("Offensives Mittelfeld")
	st := team.PlayersByPosition("Sturm")

	top2tw := tw
	if len(tw) > 2 {
		top2tw = tw[:2]
	}

	_, twAvg := overall(tw)
	_, top2twAvg := overall(top2tw)
	_, avAvg := overall(av)
	_, ivAvg := overall(iv)
	_, dmAvg := overall(dm)
	_, omAvg := overall(om)
	_, stAvg := overall(st)

	//debug logs
	log.Println(
		team.Name,
		strconv.Itoa(int(mvSum)),
		convert.FloatToHumanReadable(mvAvg),
		strconv.Itoa(int(salariesSum)),
		convert.FloatToHumanReadable(salariesAvg),
		strconv.Itoa(len(team.Players)),
		convert.FloatToHumanReadable(ageAvg),
		convert.FloatToHumanReadable(twAvg),
		convert.FloatToHumanReadable(avAvg),
		convert.FloatToHumanReadable(ivAvg),
		convert.FloatToHumanReadable(dmAvg),
		convert.FloatToHumanReadable(omAvg),
		convert.FloatToHumanReadable(stAvg),
		convert.FloatToHumanReadable(top2twAvg),
		convert.FloatToHumanReadable(t15OverallAvg),
		convert.FloatToHumanReadable(t15TalentAvg),
		convert.FloatToHumanReadable(t15AgeAvg),
		strconv.Itoa(expiringContracts),
		leagueName,
	)

	return []string{
		team.Name,
		strconv.Itoa(int(mvSum)),
		convert.FloatToHumanReadable(mvAvg),
		strconv.Itoa(int(salariesSum)),
		convert.FloatToHumanReadable(salariesAvg),
		strconv.Itoa(len(team.Players)),
		convert.FloatToHumanReadable(ageAvg),
		convert.FloatToHumanReadable(twAvg),
		convert.FloatToHumanReadable(avAvg),
		convert.FloatToHumanReadable(ivAvg),
		convert.FloatToHumanReadable(dmAvg),
		convert.FloatToHumanReadable(omAvg),
		convert.FloatToHumanReadable(stAvg),
		convert.FloatToHumanReadable(top2twAvg),
		convert.FloatToHumanReadable(t15OverallAvg),
		convert.FloatToHumanReadable(t15TalentAvg),
		convert.FloatToHumanReadable(t15AgeAvg),
		strconv.Itoa(expiringContracts),
		leagueName,
	}
}

func talent(players []onlineliga.Player) (float64, float64) {
	var values []float64
	for _, player := range players {
		values = append(values, player.Talent)
	}
	return utils.SumAvgFloat64(values)
}

func overall(players []onlineliga.Player) (float64, float64) {
	var values []float64
	for _, player := range players {
		values = append(values, player.Overall)
	}
	return utils.SumAvgFloat64(values)
}

func age(players []onlineliga.Player) (float64, float64) {
	var values []float64
	for _, player := range players {
		values = append(values, player.Age())
	}
	return utils.SumAvgFloat64(values)
}
