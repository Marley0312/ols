package onlineliga

import (
	"fmt"
	"strings"

	"github.com/PuerkitoBio/goquery"
	"github.com/tripleawwy/ols/internal/convert"
	"github.com/tripleawwy/ols/internal/status"
)

type Player struct {
	ID               int                `json:"id"`
	Name             string             `json:"name"`
	Country          string             `json:"country"`
	BirthSeason      float64            `json:"birth_season"` // 24
	BirthWeek        float64            `json:"birth_week"`   // 38
	Salary           float64            `json:"salary"`       // -
	MarketValue      float64            `json:"market_value"` // -
	Weight           float64            `json:"weight"`       // 35
	Height           float64            `json:"height"`       // 27
	Talent           float64            `json:"talent"`
	Fitness          float64            `json:"fitness"`
	Overall          float64            `json:"overall"`
	TalentDiscovered bool               `json:"talent_discovered"`
	ContractExpiring bool               `json:"contract_expiring"`
	Positions        []string           `json:"positions"`
	Abilities        map[string]float64 `json:"abilities"`
}

func playerMap(doc *goquery.Document, findQuery string) map[string]string {
	selection := doc.Find(findQuery)
	values := make(map[string]string)
	selection.Each(func(i int, s *goquery.Selection) {
		key := strings.TrimSpace(s.Children().First().Text())
		value := strings.TrimSpace(s.Children().Last().Text())
		values[key] = value
	})

	//debug logs
	// log.Println(values)

	return values
}

func PlayerProfile(player *Player, profileDoc *goquery.Document) {
	profile := playerMap(profileDoc, ".ol-player-table-row")
	abilities := playerMap(profileDoc, ".ol-playerabilitys-table-row")

	player.Name = profile["Name"]
	player.Country = profile["Nationalität"]
	player.BirthSeason = status.CurrentInformation.Season - convert.ProfileValueToFloat64(profile["Alter"], " Jahre")
	player.BirthWeek = convert.ProfileValueToFloat64(profile["Geburtstag"], "Woche ")
	player.Overall = convert.AbilityToFloat64(abilities["FÄHIGKEITEN"])
	player.Fitness = convert.AbilityToFloat64(abilities["Fitness"])
	player.Talent = convert.TalentToFloat64(abilities["Talent"])
	player.MarketValue = convert.CurrencyToFloat64(profile["Marktwert"])
	player.Salary = convert.CurrencyToFloat64(profile["Jahresgehalt"])
	player.Height = convert.ProfileValueToFloat64(profile["Grösse"], " Meter")
	player.Weight = convert.ProfileValueToFloat64(profile["Gewicht"], " kg")
	player.TalentDiscovered = !strings.Contains(abilities["Talent"], "ermittelt")
	player.ContractExpiring = strings.Contains(profile["Vertragslaufzeit"], fmt.Sprintf("Saison %v", status.CurrentInformation.Season))
	player.Positions = playerPositions(profile["Position"])
	player.Abilities = convert.AbilityMapToAbilities(abilities)

	//debug logs
	// log.Println(utils.JSONPrint(player))
}

func (player *Player) GetProfile(profileDoc *goquery.Document) {
	profile := playerMap(profileDoc, ".ol-player-table-row")
	abilities := playerMap(profileDoc, ".ol-playerabilitys-table-row")

	player.Name = profile["Name"]
	player.Country = profile["Nationalität"]
	player.BirthSeason = status.CurrentInformation.Season - convert.ProfileValueToFloat64(profile["Alter"], " Jahre")
	player.BirthWeek = convert.ProfileValueToFloat64(profile["Geburtstag"], "Woche ")
	player.Overall = convert.AbilityToFloat64(abilities["FÄHIGKEITEN"])
	player.Fitness = convert.AbilityToFloat64(abilities["Fitness"])
	player.Talent = convert.TalentToFloat64(abilities["Talent"])
	player.MarketValue = convert.CurrencyToFloat64(profile["Marktwert"])
	player.Salary = convert.CurrencyToFloat64(profile["Jahresgehalt"])
	player.Height = convert.ProfileValueToFloat64(profile["Grösse"], " Meter")
	player.Weight = convert.ProfileValueToFloat64(profile["Gewicht"], " kg")
	player.TalentDiscovered = !strings.Contains(abilities["Talent"], "ermittelt")
	player.ContractExpiring = strings.Contains(profile["Vertragslaufzeit"], fmt.Sprintf("Saison %v", status.CurrentInformation.Season))
	player.Positions = playerPositions(profile["Position"])
	player.Abilities = convert.AbilityMapToAbilities(abilities)

	//debug logs
	// log.Println(utils.JSONPrint(player))
}

func playerPositions(profileValue string) []string {
	var positions []string
	splitted := strings.Split(profileValue, ", ")
	for _, element := range splitted {
		position := element
		positions = append(positions, position)
	}
	return positions
}

func (player Player) Age() float64 {
	currentWeek := status.CurrentInformation.Week
	currentSeason := status.CurrentInformation.Season

	var roofedAge float64

	if player.BirthSeason < 0 {
		roofedAge = player.BirthSeason*-1 + currentSeason
	} else {
		roofedAge = player.BirthSeason + currentSeason
	}

	var partly float64
	if player.BirthWeek <= currentWeek {
		partly = (currentWeek - player.BirthWeek) / 44
	} else {
		partly = (player.BirthWeek - currentWeek) / 44
	}
	return roofedAge + partly
}
