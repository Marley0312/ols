package status

import (
	"encoding/json"

	"github.com/tripleawwy/ols/internal/httpclient"
	"github.com/tripleawwy/ols/internal/request"
	"github.com/tripleawwy/ols/internal/response"
	"github.com/tripleawwy/ols/internal/utils"
)

type ServerInformation struct {
	MatchdayHTML string
	MatchDay     float64
	Week         float64
	Version      string
	Season       float64
}

var CurrentInformation ServerInformation

func FetchServerInformation() {
	var statusOptions = &request.RequestOption{
		Path: "/system/info",
	}
	res := request.DoRequest(httpclient.DefaultClient, request.GET(statusOptions))
	body := response.ResponseToByte(res)

	json.Unmarshal(body, &CurrentInformation)

	//debug logs
	utils.JSONPrint(CurrentInformation)
}
