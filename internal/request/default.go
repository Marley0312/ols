package request

import (
	"fmt"
	"log"
	"net/http"
)

type Endpoint struct {
	Host   string
	Scheme string
}

var DefaultEndpoint = &Endpoint{
	Host:   "www.onlineliga.de",
	Scheme: "https",
}

var BaseURL = fmt.Sprintf("%s://%s", DefaultEndpoint.Scheme, DefaultEndpoint.Host)

func defaultGET() *http.Request {
	req, err := http.NewRequest("GET", BaseURL, nil)
	if err != nil {
		log.Fatal(err)
	}
	return req
}

func DoRequest(client *http.Client, req *http.Request) *http.Response {
	res, err := client.Do(req)
	if err != nil {
		log.Fatal(err)
	}
	return res
}
