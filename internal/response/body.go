package response

import (
	"log"
	"net/http"
)

func CloseBody(response *http.Response) {
	err := response.Body.Close()
	if err != nil {
		// dont exit when failing https://github.com/golang/go/issues/49366
		// might only be temporarily
		log.Println(err, " while closing body")
	}
}
