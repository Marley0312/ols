package utils

import (
	"encoding/json"
	"log"
)

func JSONPrettySPrint(input interface{}) string {
	jsonString, err := json.MarshalIndent(input, "", "\t")
	if err != nil {
		log.Fatal(err)
	}
	return string(jsonString)
}

func JSONSPrint(input interface{}) string {
	jsonString, err := json.Marshal(input)
	if err != nil {
		log.Fatal(err)
	}
	return string(jsonString)
}

func JSONPrettyPrint(input interface{}) {
	jsonString, err := json.MarshalIndent(input, "", "\t")
	if err != nil {
		log.Fatal(err)
	}
	log.Println(string(jsonString))
}

func JSONPrint(input interface{}) {
	jsonString, err := json.Marshal(input)
	if err != nil {
		log.Fatal(err)
	}
	log.Println(string(jsonString))
}
